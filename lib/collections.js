/**
 * 商品表
 * {
 *  _id:0   //id
 *  name:"",//商品名
 *  value:10,//价格
 *  pix:"",//base64 图片
 *  category:"",//类别
 *  weight:10,//重量
 *  measure,""  //单位
 *  IsFree:"",//是否收费
 *  actualBalance:10,//库存
 *  availableBalance:10,//有效库存
 *  IsDeleted:bool,//删除标记
 * }
 *
 * @type {Mongo.Collection}
 */
Products = new Mongo.Collection('products');

ProductCategory = new Mongo.Collection('productCategory');


///**
// * {
// * _id:"",
// * productID:"",
// * agencyID:"",
// * date:"",
// * amount:0
// * }
// **/
//Stock = new Mongo.Collection('stock');

//Products.attachSchema(new SimpleSchema({
//    pName:{
//        type:String,
//        label:"商品名",
//        max:200
//    },
//    price:{
//        type:String,
//        lable:"价格"
//    },
//    pix:{
//        lable:"图片"
//    },
//    pCategory:{
//        lable:"类别"
//    },
//    weight:{
//        lable:"重量"
//    },
//    chargable:{
//        lable:"是否收费"
//    },
//    balance:{
//        lable:"库存"
//    },
//    deleed:{
//        lable:"删除"
//    }
//}));

/**
 * 代理商
 * {
 * _id:1    //id
 * name:"",//代理商简称
 * fullName:"",//代理商全称
 * province:"",//省份
 * city:"",//城市名
 * category:10,//级别
 * address:"",//地址
 * tel:"",//电话
 * postcode:"",//邮编
 * contact:""//联系人
 * IsDeleted:bool,//删除标记
 * }
 * @type {Mongo.Collection}
 */
Agency = new Mongo.Collection('agency');

/**
 * 订单表
 *{
 *  _id:""                  //id
 *  productsID:[],      //商品ID
 *  pix:"",               //base64 图片
 *  ----quantity:100            //数量
 *  totalPrice:10,            //总价
 *  orderTotalWeight:10,        //订单总重量
 *  expressPrice:,          //运费单价
 *  IsDeleted:bool,         //删除标记
 *  AgencyID:[],             //代理商ID
 *  status:""                  //订单状态
 *  author:""               //申请人
 *  createdOn:""            //申请日期
 *  approvedDate:""         //审批日期
 *  shippingDate:""         //出货日期
 *
 * }
 * @type {Mongo.Collection}
 */
Orders = new Mongo.Collection("orders");

/**
 * 快递单价
 * {
 *      city:
 *      price:
 * }
 * @type {Mongo.Collection}
 */

ExpressPrice = new Mongo.Collection("expressPrice");

AdminUsers = new Mongo.Collection("adminUsers");

/**
 * {
 * productsID:""
 * quantity:0
 * type:"" in/out
 * author:""
 * date:""
 * }
 * @type {Mongo.Collection}
 */
Stocks = new Mongo.Collection("stocks");
/**
 * {
 *  productsID:""
 *  initQuantity:0
 *  inMonth1:
 *  outMonth1:
 *  month2Quantity:
 *  inMonth2:
 *  outMonth2:
 *  month3Quantity:
 *  inMonth3:
 *  outMonth3:
 * }
 * @type {Mongo.Collection}
 */
StocksPSI = new Mongo.Collection("stocksPSI");